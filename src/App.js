import React, { Fragment, useState, useEffect } from 'react';
import Formulario from './components/Formulario';
import Cita from './components/Cita';


function App() {

  // Citas en Local Storage
  let citasIniciales = JSON.parse(localStorage.getItem('citas'));
  if(!citasIniciales){
    citasIniciales = [];
  }

  // Creamos el state para el arreglo de citas
  const [citas, guardarCitas] = useState(citasIniciales);

  // Use Effect para realizar ciertas operaciones cuando el state cambia
  useEffect(() => {
    let citasIniciales = JSON.parse(localStorage.getItem('citas'));
    // console.log("Documento listo o algo paso con las citas")
    if (citasIniciales) {
      localStorage.setItem('citas', JSON.stringify(citas))
    } else {
      localStorage.setItem('citas',JSON.stringify([]))
    }
  },[citas])

  // Funcion que lea las citas actuales y le agrege la nueva
  const crearCita = cita => {
    guardarCitas([...citas, cita])
  }

  // Funcion para eliminar las citas por ID

  const eliminarCita = id => {
    const citasActualizadas = citas.filter(cita => cita.id !== id);
    guardarCitas(citasActualizadas);
  }

  // Mensaje condicional de las citas
  const titulo = citas.length === 0 ? 'No hay citas' : "Administra tus citas";

  return (

    <Fragment>
      <h1>Administrador de Pacientes</h1>
      <div className="container">
        <div className="row">
          <div className="one-half column">
            <Formulario
              crearCita={crearCita}
            />
          </div>
          <div className="one-half column">
            <h2>{titulo}</h2>
            {citas.map(cita => (
              <Cita
                key={cita.id}
                cita={cita}
                eliminarCita={eliminarCita}
              />
            ))}
          </div>
        </div>
      </div>


    </Fragment>
  );
}

export default App;
