import React, { Fragment, useState } from 'react';
import uuid from 'uuid/v4';
import PropTypes from 'prop-types';


const Formulario = ({crearCita}) => {

    // Creamos el state de citas
    const [cita, actualizarCita] = useState({
        mascota:'',
        propietario:'',
        fecha:'',
        hora:'',
        sintomas:'',
    })

    // Creamos el state de Errores
    const [error, actualizarError] = useState(false);

    // Funcion que vamos a ejecutar cada vez que el usuario actualice algún input
    const actualizarState = e=>{
        actualizarCita({
            ...cita,
            [e.target.name]: e.target.value
        })

    }

    // Extraer los valores

    const {mascota, propietario, fecha, hora, sintomas} = cita;

    // Cuando el usuario envia los datos del form

    const submitCita = e=>{
        e.preventDefault();

        // Validaciones
        if (mascota.trim() === "" || propietario.trim() === "" || fecha.trim() === "" || hora.trim() === "" || sintomas.trim() === "") {
            actualizarError(true);
            return;
        }

        // Eliminamos el mensaje de erro previo si lo hubiera
        actualizarError(false)
       
        // Asignar un ID
        cita.id = uuid();

        // Crear la cita
        crearCita(cita);

        // Reiniciar el Form
        actualizarCita({
            mascota:'',
            propietario:'',
            fecha:'',
            hora:'',
            sintomas:'',
        })

    }

    return (
        <Fragment>
            <h2>Datos del paciente</h2>

            {error? <p className="alerta-error">Todos los campos son obligatorios</p> : null}

            <form
                onSubmit ={submitCita}
            >
                <label>Nombre de la mascota</label>
                <input
                    type="text"
                    name="mascota"
                    className="u-full-width"
                    placeholder="Escribe el Nombre de la mascota"
                    onChange = {actualizarState}
                    value={mascota}
                />

                <label>Nombre del dueño</label>
                <input
                    type="text"
                    name="propietario"
                    className="u-full-width"
                    placeholder="Escribe el Nombre del propietario"
                    onChange = {actualizarState}
                    value={propietario}
                />

                <label>Fecha</label>
                <input
                    type="date"
                    name="fecha"
                    className="u-full-width"
                    onChange = {actualizarState}
                    value={fecha}
                />

                <label>Hora</label>
                <input
                    type="time"
                    name="hora"
                    className="u-full-width"
                    onChange = {actualizarState}
                    value={hora}
                />

                <label>Síntomas</label>
                <textarea
                    className="u-full-width"
                    name="sintomas"
                    onChange = {actualizarState}
                    value={sintomas}
                ></textarea>

                <button
                    type="submit"
                    className="u-full-width button-primary"
                >Agregar Cita</button>
            </form>

        </Fragment>
    );
}

Formulario.propTypes = {
    crearCita: PropTypes.func.isRequired
}

export default Formulario;